package main

import (
	"encoding/gob"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

// loadTable fetches the CDF table for the markov chain and loads it into memory
func loadTable(filename string) ([26][26]float64, error) {
	var cdfTable [26][26]float64
	fp, err := os.Open(filename)
	if err != nil {
		return cdfTable, err
	}
	err = gob.NewDecoder(fp).Decode(&cdfTable)
	return cdfTable, err
}

func GenerateTag(length int, table [26][26]float64) string {
	rand.Seed(time.Now().UTC().UnixNano())
	tagASCII := make([]int, length)
	tagASCII[0] = rand.Intn(26)
	for i := 1; i < length; i++ {
		value := rand.Float64()
		fmt.Println(value)
		for charIdx, cumulativeProb := range table[tagASCII[i-1]] {
			if value <= cumulativeProb {
				tagASCII[i] = charIdx
				break
			}
		}
	}
	var tagBuf strings.Builder
	for _, letter := range tagASCII {
		fmt.Fprintf(&tagBuf, "%c", letter+int('a'))
	}
	return tagBuf.String()
}

func main() {
	table, err := loadTable("gutenberg_corpus.gob")
	if err != nil {
		panic(err)
	}
	fmt.Println(GenerateTag(80, table))
}
