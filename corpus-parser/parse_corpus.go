package main

import (
    "sync"
    "encoding/gob"
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

// GenerateDigraphTable takes a file and counts every character-pair cooccurrence 
func GenerateDigraphTable(fp *os.File, results chan [26][26]uint64, wg *sync.WaitGroup) {
	localDigraphTable := [26][26]uint64{}
	offset := uint64('a')
	for scanner := bufio.NewScanner(fp); scanner.Scan(); {
		for _, word := range strings.Fields(scanner.Text()) {
			if len(word) > 1 {
				for i := 1; i < len(word); i++ {
					srcLetterIdx := uint64(word[i-1]) - offset
					dstLetterIdx := uint64(word[i]) - offset
					localDigraphTable[srcLetterIdx][dstLetterIdx]++
				}
			}
		}
	}
    results <- localDigraphTable
    wg.Done()
}

// CountToMarkov takes the count table and makes a CDF out of each row
func CountToMarkov( counts [26][26]uint64) [26][26]float64 {
    cdfTable := [26][26]float64{}
	for i := range counts {
		var rowSum float64
		rowSum = 0.0
		for j := range counts[i] {
			rowSum += float64(counts[i][j])
		}
        for j := range counts[i] {
            if j == 0 { 
                cdfTable[i][j] = float64(counts[i][j]) / rowSum
            } else {
                cdfTable[i][j] = (float64(counts[i][j]) / rowSum) + cdfTable[i][j-1]
            }
        }
	}
    return cdfTable
}

// SaveTableToFile writes the transition matrix to a file
func SaveTableToFile(filename string, table [26][26]float64) error {
    fp, err := os.Create(filename)
    if err != nil {
        return err
    }
    defer fp.Close()
    gob.NewEncoder(fp).Encode(table)
    return nil
}

func main() {
	flag.Parse()
	corpusFileNames := flag.Args()
	filepointers := make([]*os.File, len(corpusFileNames))
	for i, filename := range corpusFileNames {
		fp, err := os.Open(filename)
		if err != nil {
			fmt.Printf("Error opening file: %v\n", filename)
			panic(err)
		} else {
            filepointers[i] = fp
		}
	}
	results := make(chan [26][26]uint64, len(filepointers))
    var wg sync.WaitGroup
    wg.Add(len(filepointers))
	for _, filepointer := range filepointers {
        go GenerateDigraphTable(filepointer, results, &wg)
	}
    wg.Wait()
    close(results)
	summedDigraphTable := [26][26]uint64{}
	for table := range results {
		for i := range table {
			for j := range table[i] {
				summedDigraphTable[i][j] += table[i][j]
			}
		}
	}
    markovTable := CountToMarkov(summedDigraphTable)
    err := SaveTableToFile("../gutenberg_corpus.gob", markovTable)
    if err != nil {
        panic(err)
    }
}
